/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.maven;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.mockito.Mockito;

public class TrackMojoTest extends AbstractMojoTestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void testDefaultPort() throws Exception {
        TrackMojo mojo = Mockito.spy((TrackMojo) lookupMojo("track", new File(getClass().getClassLoader().getResource("default-pom.xml").getPath())));
        assertNotNull(mojo);

        assertNull(mojo.getHost());
        assertNull(mojo.getPort());
    }

    public void testCustomHostAndPort() throws Exception {
        TrackMojo mojo = (TrackMojo) lookupMojo("track", new File(getClass().getClassLoader().getResource("host-port-pom.xml").getPath()));
        assertNotNull(mojo);

        assertEquals("ciccio", mojo.getHost());
        assertEquals("1000", mojo.getPort());
    }

    public void testCustomUrlIgnoreHostAndPort() throws Exception {
        TrackMojo mojo = (TrackMojo) lookupMojo("track", new File(getClass().getClassLoader().getResource("url-pom.xml").getPath()));
        assertNotNull(mojo);

        assertEquals("pluto", mojo.getUrl());
        assertEquals("ciccio", mojo.getHost());
        assertEquals("1000", mojo.getPort());
    }

    public void testCustomOutput() throws Exception {
        TrackMojo mojo = (TrackMojo) lookupMojo("track", new File(getClass().getClassLoader().getResource("output-pom.xml").getPath()));
        assertNotNull(mojo);

        assertEquals("target/ciccio-tracker/ciccio-memory-file.xml", mojo.getOutput());
    }
}
