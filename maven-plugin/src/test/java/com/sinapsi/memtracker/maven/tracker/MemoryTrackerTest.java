/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.maven.tracker;

import static com.sinapsi.memtracker.maven.tracker.MemoryTracker.DEFAULT_DIR;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.lang.management.MemoryPoolMXBean;

import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MemoryTrackerTest {

    private MemoryTracker tracker;
    private CompositeData usage;

    @BeforeMethod
    public void mockRemoteJmx() throws Exception {
        tracker = Mockito.spy(new MemoryTracker());
        doReturn(mock(JMXConnector.class)).when(tracker).getConnection();
        usage = mock(CompositeData.class);
        doReturn(usage).when(tracker).getRemoteUsage(any(JMXConnector.class), any(MemoryPoolMXBean.class));
    }

    @Test
    public void testSaveRemoteWithNullValuesDoesNotThrowsException() throws Exception {
        tracker.saveRemote();
    }

    @Test
    public void testSaveRemote() throws Exception {
        when(usage.get("init")).thenReturn(1);
        when(usage.get("used")).thenReturn(1);
        when(usage.get("committed")).thenReturn(1);
        when(usage.get("max")).thenReturn(1);
        tracker.saveRemote();
        assertTrue(new File(DEFAULT_DIR).exists());
        assertTrue(new File(DEFAULT_DIR + "memory-usage.xml").isFile());
    }

    @Test
    public void testSaveRemoteWithDifferentOutput() throws Exception {
        when(usage.get("init")).thenReturn(1);
        when(usage.get("used")).thenReturn(1);
        when(usage.get("committed")).thenReturn(1);
        when(usage.get("max")).thenReturn(1);
        tracker.setOutput("target/ciccio-tracker/ciccio-memory-file.xml");
        tracker.saveRemote();
        assertTrue(new File("target/ciccio-tracker/").exists());
        assertTrue(new File("target/ciccio-tracker/ciccio-memory-file.xml").isFile());
    }
}
