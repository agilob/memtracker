/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.maven;

import com.sinapsi.memtracker.maven.tracker.MemoryTracker;

/**
 * @goal track
 * @phase post-integration-test
 * @author naaka
 */
public class TrackMojo extends AbstractMemTrackerMojo {

    @Override
    public final void executeMojo() throws Exception {
        MemoryTracker tracker = null;
        if (url != null) {
            log.debug("tracking memory usage using custom jmx url {}...", url);
            tracker = new MemoryTracker(url);
        } else {
            if (host != null && port != null) {
                log.debug("tracking memory usage using custom jmx host {} and port {}...", host, port);
                tracker = new MemoryTracker(host, port);
            } else {
                log.debug("tracking memory usage using default settings...");
                tracker = new MemoryTracker();
            }
        }

        if (output != null) {
            tracker.setOutput(output);
        }
        
        tracker.saveRemote();
    }
}