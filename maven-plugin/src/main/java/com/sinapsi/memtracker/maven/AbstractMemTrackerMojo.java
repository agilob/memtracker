/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.maven;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base mojo for mem tracker execution.<br/>
 * it does the following:
 * <ul>
 * <li>execute the mojo</li>
 * </ul>
 * 
 * @author naaka
 */
public abstract class AbstractMemTrackerMojo extends AbstractMojo {

    protected static Logger log = LoggerFactory.getLogger(AbstractMemTrackerMojo.class);

    /** @parameter */
    protected String host;
    
    /** @parameter */
    protected String port;
    
    /** @parameter */
    protected String url;
    
    /** @parameter */
    protected String output;
    
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            log.debug("------------------------");
            log.debug("MEMORY TRACKER");
            log.debug("------------------------");

            executeMojo();

        } catch (Exception e) {
            log.error("ERROR IN MEMORY TRACKER !!!", e);
        }

    }

    public abstract void executeMojo() throws Exception;


    public String getHost() {
        return host;
    }
    
    public String getPort() {
        return port;
    }
    
    public String getUrl() {
        return url;
    }
    
    public String getOutput() {
        return output;
    }
}
