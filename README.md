Sonar Plugin Usage
=
Introduction
=

The Memtracker Sonar Plugin reports memory data collected by the maven
plugin to Sonar.
The reported metrics are:

* Pern Gen
* Heap Space

The metrics are reported in MB for ease of reading.

Usage
====

Just download and install the plugin jar in Sonar.


Maven Plugin Usage
=

Introduction
=

The Memtracker Maven Plugin tracks peak memory usage during integration testing.
To do so the Memtracker connects to a remote JVM through JXM.

The data are taken from the memory pool mx beans:
`ManagementFactory.getMemoryPoolMXBeans()`

The data tracked are the peak memory usages:
`MemoryPoolMXBean.getPeakUsage()`

The operation is repeated for non heap memory beans (`Code Cache` and `Perm Gen`) 
and heap memory beans (the `Young`, `Tenured` and `Old` generations).
Memory usage is tracked as is (in bytes).


Separate Usage
===
The plugin can be used separately from the Sonar plugin. To launch it, use:
`mvn mem-tracker:track`


The correct execution output is the following:
```
[INFO] [mem-tracker:track {execution: integration-test}]
DEBUG AbstractMemTrackerMojo.execute(java:33)   ------------------------
DEBUG AbstractMemTrackerMojo.execute(java:34)   MEMORY TRACKER
DEBUG AbstractMemTrackerMojo.execute(java:35)   ------------------------
DEBUG TrackMojo.executeMojo(java:22)   tracking memory usage using default settings...
INFO  MemoryTracker.saveRemote(java:57)   tracked memory usage for server [localhost] on port [10088]:
INFO  MemoryTracker.saveRemote(java:59)   Memory[Non-heap memory,Code Cache,49152K]
INFO  MemoryTracker.saveRemote(java:59)   Memory[Heap memory,PS Eden Space,54912K]
INFO  MemoryTracker.saveRemote(java:59)   Memory[Heap memory,PS Survivor Space,9920K]
INFO  MemoryTracker.saveRemote(java:59)   Memory[Heap memory,PS Old Gen,466048K]
INFO  MemoryTracker.saveRemote(java:59)   Memory[Non-heap memory,PS Perm Gen,131072K]
```


Default Configuration
=====================
```
<configuration>
    <host>localhost</host>
    <port>10088</port>
    <output>target/mem-tracker/memory-usage.xml</output>
</configuration>
```

Custom Configuration
====================
```
<plugin>
    <groupId>com.sinapsi.memtracker</groupId>
    <artifactId>mem-tracker-maven-plugin</artifactId>
    ...
    <configuration>
        <host>ciccio</host>
        <port>1000</port>
        <output>target/ciccio-tracker/ciccio-memory-file.xml</output>
    </configuration>
</plugin>
```


Sample Memory File
==================
```
<linked-list>
  <memory>
    <poolName>Code Cache</poolName>
    <type>NON_HEAP</type>
    <memoryUsage>
      <init>2359296</init>
      <used>11724608</used>
      <committed>11763712</committed>
      <max>50331648</max>
    </memoryUsage>
  </memory>
  <memory>
    <poolName>PS Eden Space</poolName>
    <type>HEAP</type>
    <memoryUsage>
      <init>22478848</init>
      <used>52953088</used>
      <committed>52953088</committed>
      <max>56229888</max>
    </memoryUsage>
  </memory>
  <memory>
    <poolName>PS Survivor Space</poolName>
    <type>HEAP</type>
    <memoryUsage>
      <init>3670016</init>
      <used>7851976</used>
      <committed>10158080</committed>
      <max>10158080</max>
    </memoryUsage>
  </memory>
  <memory>
    <poolName>PS Old Gen</poolName>
    <type>HEAP</type>
    <memoryUsage>
      <init>238616576</init>
      <used>60959296</used>
      <committed>238616576</committed>
      <max>477233152</max>
    </memoryUsage>
  </memory>
  <memory>
    <poolName>PS Perm Gen</poolName>
    <type>NON_HEAP</type>
    <memoryUsage>
      <init>67108864</init>
      <used>65135160</used>
      <committed>116654080</committed>
      <max>134217728</max>
    </memoryUsage>
  </memory>
</linked-list>
```

Sinapsi Repository
==================
```
    <repositories>
        <repository>
            <id>repository.sinapsi</id>
            <name>sinapsi repository</name>
            <url>http://air.sinapsi.com:8080/artifactory/libs-releases-local/
            </url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>
```
[![piwik](https://agilob.net/piwik/piwik.php?idsite=5&rec=1)](https://agilob.net/) 