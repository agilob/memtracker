/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.sonar;

import static com.sinapsi.memtracker.sonar.MemTrackerMetrics.HEAP_OCCUPATION;
import static com.sinapsi.memtracker.sonar.MemTrackerMetrics.PERM_GEN_OCCUPATION;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import org.sonar.api.batch.SensorContext;
import org.sonar.api.resources.Project;
import org.testng.annotations.Test;

public class MemTrackerSensorTest {

    @Test
    public void shouldGetReportPathFromProperty() {
        SensorContext context = mock(SensorContext.class);
        Project project = new ProjectLoader().loadFromPom(getClass(), "readMemoryOccupationFromFile/pom.xml");

        new MemTrackerSensor().analyse(project, context);
        verify(context, atLeastOnce()).saveMeasure(eq(HEAP_OCCUPATION), anyDouble());
        verify(context, atLeastOnce()).saveMeasure(eq(PERM_GEN_OCCUPATION), anyDouble());
    }

    @Test
    public void shouldNotFailWhenReportNotFound() {
        SensorContext context = mock(SensorContext.class);
        Project project = new ProjectLoader().loadFromPom(getClass(), "shouldNotFailWhenReportNotFound/pom.xml");

        new MemTrackerSensor().analyse(project, context);
        verifyZeroInteractions(context);
    }

    @Test
    public void shouldNotFailWhenReportIsWrong() {
        SensorContext context = mock(SensorContext.class);
        Project project = new ProjectLoader().loadFromPom(getClass(), "shouldNotFailWhenReportIsWrong/pom.xml");

        new MemTrackerSensor().analyse(project, context);
        verifyZeroInteractions(context);
    }
}
