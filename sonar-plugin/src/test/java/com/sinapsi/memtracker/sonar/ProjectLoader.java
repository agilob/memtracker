/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 
 */
package com.sinapsi.memtracker.sonar;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.sonar.api.test.MavenTestUtils.loadProjectFromPom;

import java.io.File;

import org.mockito.Mockito;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.ProjectFileSystem;

public class ProjectLoader {

    public Project loadFromPom(Class<?> clazz, String pom) {
        Project project = spy(loadProjectFromPom(clazz, pom));

        // mock file build dir
        ProjectFileSystem fileSystem = Mockito.spy(project.getFileSystem());
        File wrongBuildDir = project.getFileSystem().getBuildDir();
        when(fileSystem.getBuildDir()).thenReturn(new File(wrongBuildDir.getAbsolutePath() + "/target"));

        // mock file system
        when(project.getFileSystem()).thenReturn(fileSystem);
        return project;
    }
}