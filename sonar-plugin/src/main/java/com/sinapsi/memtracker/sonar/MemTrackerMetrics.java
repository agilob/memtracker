/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.sonar;

import java.util.ArrayList;
import java.util.List;

import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

public class MemTrackerMetrics implements Metrics {

    public final static Metric HEAP_OCCUPATION = new Metric("heap-occupation", "Heap (Mb)", "Heap", Metric.ValueType.INT, Metric.DIRECTION_WORST, false, CoreMetrics.DOMAIN_SIZE);
    public final static Metric PERM_GEN_OCCUPATION = new Metric("permgen-occupation", "PermGen (Mb)", "PermGen", Metric.ValueType.INT, Metric.DIRECTION_WORST, false, CoreMetrics.DOMAIN_SIZE);

    public List<Metric> getMetrics() {
        ArrayList<Metric> metrics = new ArrayList<Metric>();
        metrics.add(HEAP_OCCUPATION);
        metrics.add(PERM_GEN_OCCUPATION);
        return metrics;
    }

}
