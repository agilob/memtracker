/*
 * Copyright (C) 2010 Sinapsi Spa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinapsi.memtracker.sonar;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.Extension;
import org.sonar.api.Plugin;

public class MemTrackerPlugin implements Plugin {

    public static final String KEY = "memorytracker";

    public String getDescription() {
        return "Monitor project memory consumption during integration test";
    }

    @SuppressWarnings("unchecked")
    public List<Class<? extends Extension>> getExtensions() {
        return Arrays.asList(MemTrackerPluginHandler.class, MemTrackerMetrics.class, MemTrackerSensor.class, MemTrackerWidget.class);
    }

    public String getKey() {
        return KEY;
    }

    public String getName() {
        return "Memory Tracker";
    }

}